﻿/*
Необходимо создать БД, которая содержит следующие таблицы:
- developers (хранит данные о разработчиках)
- skills (навыки разработчиков – Java, C++, etc.)
- projects (проекты, на которых работают разработчики)
- companies (IT компании, в которых работают разработчики)
- customers (клиенты, которые являются заказчиками проектов в IT компаниях)
При этом:
- разработчики могут иметь 2 недели много навыков
- каждый проект имеет много разработчиков, которые над ним работают
- компании выполняют много проектов одновременно - заказчики имеют много проектов

Необходимо реализовать как таблицы, так и грамотные связи между ними.

Результатом выполнения задания являеются файлы initDB.sql (создание таблиц и связей между ними), populateDB.sql (заполнение таблиц данными)*/

create table developers(
 ID int primary key not null,
 NAME varchar(25) not null,
 PROJECT int,
 COMPANY int
);
create table skills(
ID INT PRIMARY KEY NOT NULL,
SKILL VARCHAR(20)
);
create table projects(
ID INT PRIMARY KEY NOT NULL,
PROJECT VARCHAR(20) NOT NULL
);
create table customers(
ID INT PRIMARY KEY NOT NULL,
CUSTOMER VARCHAR(50) NOT NULL 
);
create table companies(
ID INT PRIMARY KEY NOT NULL,
NAME VARCHAR(50) NOT NULL
);
create table developers_skills(
ID INT PRIMARY KEY NOT NULL,
DEVELOPER INT NOT NULL,
SKILL INT
);


alter table projects add customer int;
alter table projects add constraint projects_id_fk foreign key (customer) references customers (id);

alter table developers add constraint developers_id_fk foreign key (project) references projects (id);
alter table developers add constraint developers_id_fk2 foreign key (company) references companies (id);

alter table developers_skills add constraint developers_skills_fk foreign key (skill) references skills (id);
alter table developers_skills add constraint developers_skills_fk2 foreign key (developer) references developers (id);

commit;


