﻿--Добавить разаработчикам поле (salary - зарплата). 

alter table developers add column salary int;  

update developers set salary = 10000 where id<3;
update developers set salary = 20000 where id>=3 and id<5;
update developers set salary = 10000 where id>=5 and id<7;
update developers set salary = 10000 where id>=7 and id<9;
update developers set salary = 10000 where id>=9 and id<11;
update developers set salary = 10000 where id>=11;

commit;