﻿--Добавить поле (cost - стоимость) в таблицу Projects .
alter table projects add column cost money;

update projects
set cost=56000 where id=1;
update projects
set cost=21000 where id=2;
update projects
set cost=78000 where id=3;
update projects
set cost=42000 where id=4;
update projects
set cost=96000 where id=5;
update projects
set cost=15000 where id=6;
update projects
set cost=7000 where id=7;
update projects
set cost=39000 where id=8;

commit;